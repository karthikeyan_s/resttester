package com.resttester.controller;

import com.resttester.data.Tweet;
import com.resttester.service.TweetHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Created by ksubbaraj on 11/12/14.
 */
@Controller
public class TweetController {

    private TweetHandler tweetHandler;

    @Autowired
    public TweetController(TweetHandler tweetHandler) {
        this.tweetHandler = tweetHandler;
    }

    @RequestMapping(value = "/tweets", method = RequestMethod.POST)
    @ResponseBody
    public Integer addTweet(@RequestBody Map tweetMap) {
        return tweetHandler.persistTweet(tweetMap);
    }

    @RequestMapping(value = "/tweets", method = RequestMethod.GET)
    @ResponseBody
    public List<Tweet> getTweets(@RequestParam(value = "offset") Integer offset, @RequestParam(value = "limit") Integer limit) {
        return tweetHandler.getTweets(offset, limit);
    }

    @RequestMapping(value = "/tweets/{tweet_id}", method = RequestMethod.PUT)
    @ResponseBody
    public Integer updateTweet(@RequestBody Map tweetMap, @PathVariable(value = "tweet_id") Integer id) {
        return tweetHandler.updateTweet(id, tweetMap);
    }

    @RequestMapping(value = "/tweets/{tweet_id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteTweet(@PathVariable(value = "tweet_id") Integer id) {
        tweetHandler.deleteTweet(id);
    }

    @RequestMapping(value = "/tweets/search", method = RequestMethod.GET)
    @ResponseBody
    public List<Tweet> getSearchedTweets(@RequestParam(value = "query") String query) {
        return tweetHandler.searchTweets(query);
    }
}
