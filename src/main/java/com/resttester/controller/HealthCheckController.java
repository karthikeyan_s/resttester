package com.resttester.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by ksubbaraj on 11/12/14.
 */

@Controller
public class HealthCheckController {

    @RequestMapping("/hc")
    @ResponseBody
    public String hc() {
        return "I am alive";
    }
}
