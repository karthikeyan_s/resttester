package com.resttester.data;

/**
 * Created by ksubbaraj on 11/12/14.
 */
public class Tweet {

    private String content;
    private int id = 0;
    public Tweet() {

    }

    public Tweet(int id, String content) {
        this.content = content;
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
