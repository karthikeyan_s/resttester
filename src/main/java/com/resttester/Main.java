package com.resttester;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.session.SessionHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.SessionTrackingMode;
import java.util.HashSet;

/**
 * Created by ksubbaraj on 11/12/14.
 */
public class Main {

    public static void main(String[] args) throws Exception {
        Server server = new Server(6789);
        BasicConfigurator.configure();

        ServletContextHandler servletContext = new ServletContextHandler(server, "/");
        servletContext.setSessionHandler(new SessionHandler());

        ServletHolder servletHolder = new ServletHolder(DispatcherServlet.class);
        servletHolder.setInitParameter("contextConfigLocation", "classpath:/context.xml");
        servletHolder.setInitOrder(1);
        servletContext.addServlet(servletHolder, "/*");

        server.setHandler(servletContext);

        server.start();

        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                Logger.getLogger(getClass()).info("System is gonna halt");
            }
        }));
    }
}
