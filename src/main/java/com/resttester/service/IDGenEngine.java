package com.resttester.service;

import org.springframework.stereotype.Service;

/**
 * Created by ksubbaraj on 11/12/14.
 */

@Service
public class IDGenEngine {

    private int lastUsedId;

    public IDGenEngine() {
        lastUsedId = retrieveLastUsedID();
    }

    private int retrieveLastUsedID() {
        // Retrieve from DB.
        return 0;
    }

    public synchronized int getNextIDForTweet() {
        return ++lastUsedId;
        // Persist in DB, at least when the object is deleted ..
    }
}
