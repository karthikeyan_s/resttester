package com.resttester.service;

import com.resttester.data.Tweet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by ksubbaraj on 11/12/14.
 */
@Service
public class TweetHandler {

    @Autowired IDGenEngine idGenEngine;

    private Map<Integer, Tweet> tweetStore = new TreeMap<Integer, Tweet>();

    public Integer persistTweet(Map tweetMap) {
        int idForTweet = idGenEngine.getNextIDForTweet();
        Tweet tweet = new Tweet(idForTweet, (String)tweetMap.get("content"));
        tweetStore.put(idForTweet, tweet);
        return idForTweet;
    }

    public List<Tweet> getTweets(Integer offset, Integer limit) {
        List<Tweet> listOfTweets = new ArrayList<Tweet>();
        for(int i = 0; i < tweetStore.size(); i++) {
            if(i >= offset && i < (offset + limit)) {
                Tweet tweet = tweetStore.get(i+1);
                listOfTweets.add(tweet);
            }
        }

        return listOfTweets;
    }

    public Integer updateTweet(int idForTweet,  Map tweetMap) {
        Tweet tweet = new Tweet(idForTweet, (String)tweetMap.get("content"));
        tweetStore.put(idForTweet, tweet);
        return idForTweet;
    }

    public void deleteTweet(Integer id) {
        tweetStore.remove(id);
    }

    public List<Tweet> searchTweets(String query) {
        List<Tweet> listOfTweets = new ArrayList<Tweet>();
        for(Integer id : tweetStore.keySet()) {
            Tweet tweet = tweetStore.get(id);
            if(tweet.getContent().contains(query)) {
                listOfTweets.add(tweet);
            }
        }
        return listOfTweets;
    }
}
